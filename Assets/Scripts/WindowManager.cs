﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowManager : MonoBehaviour
{

    public GameObject panelInicio;
    public GameObject panelAgregar;
    public GameObject panelPropiedadesP;
    public GameObject panelPropiedadesE;
    public GameObject panelControles;
    public GameObject panelCamara;
    public GameObject panelConfirmacion;

    public GameObject orbitPrefab;

    private void Awake()
    {
        if (!panelInicio)
            Debug.LogWarning(panelInicio + " not assigned.");
        if (!panelAgregar)
            Debug.LogWarning(panelAgregar + " not assigned.");
        if (!panelPropiedadesP)
            Debug.LogWarning(panelPropiedadesP + " not assigned.");
        if (!panelPropiedadesE)
            Debug.LogWarning(panelPropiedadesE + " not assigned.");
        if (!panelControles)
            Debug.LogWarning(panelControles + " not assigned.");
        if (!panelCamara)
            Debug.LogWarning(panelCamara + " not assigned.");
        if (!panelConfirmacion)
            Debug.LogWarning(panelConfirmacion + " not assigned.");
    }

    public void Start()
    {
        panelInicio.SetActive(true);
        panelAgregar.SetActive(false);
        panelPropiedadesP.SetActive(false);
        panelPropiedadesE.SetActive(false);
        panelControles.SetActive(false);
        panelCamara.SetActive(false);
        panelConfirmacion.SetActive(false);
    }

    public void ShowStartupControls()
    {
        panelInicio.SetActive(false);

        panelAgregar.SetActive(true);
        panelCamara.SetActive(true);

        orbitPrefab.SetActive(false);
    }

    public void ShowControlPanels()
    {
        panelPropiedadesE.SetActive(true);
    }

}
