﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraAnimator : MonoBehaviour
{

    private Animator animator;
    //private bool isHidden;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void showPanel()
    {
        animator.ResetTrigger("hideCamera");
        animator.SetTrigger("showCamera");
    }

    public void hidePanel()
    {
        animator.ResetTrigger("showCamera");
        animator.SetTrigger("hideCamera");
    }

}
