﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarPropertiesManager : MonoBehaviour
{
    public InputField massInput;
    public InputField radiusInput;
    public InputField temperatureInput;
    public Slider scaleSlider;

    public void ChangeRadius()
    {
        float r = scaleSlider.value;
        if (r == 0)
            r = 0.05f;

        if (CosmosManager.currentObject.GetComponentInChildren<ParticleSystem>())
        {
            CosmosManager.currentObject.GetComponentInChildren<ParticleSystem>().startSize = scaleSlider.value * 2;

            ParticleSystem ps = CosmosManager.currentObject.GetComponentInChildren<ParticleSystem>();
            ParticleSystem.ShapeModule x = ps.shape;

            x.radius = scaleSlider.value * 0.52f;
        }
        CosmosManager.currentObject.transform.localScale = new Vector3(r, r, r);
    }

}
