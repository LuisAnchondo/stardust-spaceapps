﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour
{
    // Perihelio
    public float p;
    // Afelio
    public float a;

    // Semi ejes
    // Mayor
    private float sA;
    // Menor
    private float sB;
    // Focal
    private float sC;

    // k
    private float h;
    

    // x
    private float x;

    public float incremento;

    //Masas
    //private float masaSol;
    //private float masaTierra;


    // Constante gravitacional de Newton
    private float cGravitacional = 6.67f * Mathf.Pow(10, -11);

    public GameObject sol;
    public GameObject tierra;

    private bool incr = true;

    // Start is called before the first frame update
    void Start()
    {
        //p = 10f;
        //a = 20f;

        x = -p;

        print("x inicial = " + x);

        resB();
        resA();
        resC();
        resH();

        print("semieje menor = " + sB);
        print("semieje mayor = " +sA);
        print("semieje focal = " + sC);
        print("h = " + h);

        incr = true;
    }

    private float limiteX = 0;
    private float limiteZ = 0;

    // Update is called once per frame
    void FixedUpdate()
    {
        if(incr)
        {
            tierra.transform.localPosition = new Vector3(x, tierra.transform.position.y, TrayectoriaZ(x));

            x+= incremento;

            if (x > a)
            {
               // print("Superior = " + tierra.transform.position.x + " | x Actual = " + x);
                if (limiteX == 0)
                    limiteX = tierra.transform.position.x;
                x -= incremento;
                incr = !incr;
                return;
            }
        }
        if(!incr)
        {
            tierra.transform.localPosition = new Vector3(x, tierra.transform.position.y, -TrayectoriaZ(x));

            x -= incremento;

            if (x < -p)
            {
                //print("Inferior = " + tierra.transform.position.x + " | x Actual = " + x);
                if (limiteZ == 0)
                    limiteZ = tierra.transform.position.z;
                x += incremento;
                incr = !incr;
                return;
            }
        }

        //print(x);
        
    }

    private void resB()
    {
        sB = Mathf.Sqrt(p*a);
    }

    private void resA()
    {
        sA = (a + p) / 2;
    }

    private void resC()
    {
        sC = Mathf.Sqrt(Mathf.Pow(sA, 2)- Mathf.Pow(sB, 2));
    }

    private void resH()
    {
        h = - sC;
    }

    public float TrayectoriaZ(float z)
    {
        float posZ = 0;

        posZ = sB * Mathf.Sqrt((1 - Mathf.Pow((x+h)/sA, 2)));

        return posZ;
    }

}
