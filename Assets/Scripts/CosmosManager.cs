﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CosmosManager : MonoBehaviour
{
    public GameObject sunPrefab;
    public GameObject planetPrefab;
    public GameObject orbitPrefab;
    public GameObject sphereHolder;
    public static GameObject currentObject;

    public List<GameObject> allObjects = null;

    public void CreateSun()
    {
        HideAllObjects();
        sunPrefab = Instantiate(sunPrefab, sphereHolder.transform, true);
        sunPrefab.transform.localPosition = Vector3.zero;
        sunPrefab.SetActive(true);
        allObjects.Add(sunPrefab);
        currentObject = sunPrefab;
    }

    public void CreatePlanet()
    {
        HideAllObjects();
        planetPrefab = Instantiate(planetPrefab, sphereHolder.transform, true);
        planetPrefab.transform.localPosition = Vector3.zero;
        planetPrefab.SetActive(true);
        allObjects.Add(planetPrefab);
        currentObject = planetPrefab;
    }

    private void HideAllObjects()
    {
        if (allObjects.Count == 0)
            return;
        for (int i = 0; i < allObjects.Count; i++)
        {
            allObjects[i].SetActive(false);
        }
    }

    public void DeleteCurrentObject()
    {
        Destroy(currentObject);
    }

    public void ViewOrbit()
    {
        orbitPrefab.SetActive(true);
    }

}
