﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAnimations : MonoBehaviour
{
    public Animator animator;
    private bool isHidden = true;

    public void ToggleMenu()
    {
        if (isHidden)
        {
            animator.ResetTrigger("HideMenu");
            animator.SetTrigger("ShowMenu");

            isHidden = false;
        }
        else
        {
            animator.ResetTrigger("ShowMenu");
            animator.SetTrigger("HideMenu");

            isHidden = true;
        }
        
    }
}
